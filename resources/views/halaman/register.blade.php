<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
    <body>
        <h1>Buat Akun Baru!</h1>
        <h3>Sign Up Form</h3>
    
        <form action="/welcome" method="POST">
        @csrf
        <p>First Name: </p>
            <input type="text" name="nama_depan:">
        <p>Last Name: </p>
            <input type="text" name="nama_belakang:">
        
        <p>Gender </p>
            <input type="radio" name="gender" value="M"> Male <br>
            <input type="radio" name="gender" value="F"> Female <br>
            <input type="radio" name="gender" value="O"> Other
          
        <p>Nationality</p>
            <select name="negara">
                <option value="1">Indonesian</option>
                <option value="2">America</option>
                <option value="3">Inggris</option>
            </select>
    
        <p>Language Spoken</p>
            <input type="checkbox" name="bahasa">Bahasa Indonesia<br>
            <input type="checkbox" name="bahasa">English<br>
            <input type="checkbox" name="bahasa">Other</br>
        
        <p>Bio:</p>
        <textarea cols="30" rows="10"></textarea><br>
        
    
        <input type="submit" value="Sign Up">
    </form>


</body>
</html>